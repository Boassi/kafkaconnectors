package br.com.itau.kafkaconnector;

import java.util.Properties;
import java.util.UUID;

import br.com.itau.kafka.consumidor.Consumidor;

public class App 
{
    public static void main( String[] args ) throws Exception
    {
//    	  Properties props = new Properties();
//	      props.put("bootstrap.servers", "localhost:9092");
//	      props.put("acks", "all");
//	      props.put("retries", 0);
//	      props.put("batch.size", 16384);
//	      props.put("linger.ms", 1);
//	      props.put("buffer.memory", 33554432);
//	      props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//	      props.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
//	      ProdutorJson produtor = new ProdutorJson("testekafka",props);
//	      Lancamento lancamento = new Lancamento(UUID.randomUUID(),100.00);	      
//	      produtor.send(lancamento);  
//	      lancamento = new Lancamento(UUID.randomUUID(),200.00);	      
//	      produtor.send(lancamento);
//	      lancamento = new Lancamento(UUID.randomUUID(),300.00);	      
//	      produtor.send(lancamento);
//	      lancamento = new Lancamento(UUID.randomUUID(),400.00);	      
//	      produtor.send(lancamento);
//    	
    	
    	Properties props = new Properties();
    	props.put("bootstrap.servers", "localhost:9092");
    	props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
    	props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
    	props.put("group.id", UUID.randomUUID().toString());
	    props.put("enable.auto.commit", "true");
	    props.put("auto.commit.interval.ms", "1000");
	    props.put("session.timeout.ms", "30000");
	    props.put("auto.offset.reset", "earliest");
	    Consumidor consumidor = new Consumidor("testekafka",props);
	    consumidor.runconsumer();
        
    }
}
