package br.com.itau.kafka.consumidor;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.kafka.model.Lancamento;

public class Consumidor extends Thread {

	private static String topic;
	private static Properties props;
	private static ObjectMapper mapper;

	public Consumidor(String topic, Properties props) {
		this.props = props;
		this.topic = topic;
	}
	public static Consumer<String,String> createconsumer()
	{
		KafkaConsumer<String, String> consumer = new KafkaConsumer<String,String>(props);
		consumer.subscribe(Collections.singletonList(topic));
		return consumer;
		
	}
	public static void runconsumer() throws InterruptedException {
		final Consumer<String, String> consumer = createconsumer();
		final int giveUp = 100;
		int noRecordsCount = 0;
        while (true) {
            final ConsumerRecords<String, String> consumerRecords = consumer.poll(1000);

            if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            consumerRecords.forEach(record -> {
                System.out.printf("Consumer Record:(%d, %s, %d, %d)\n",
                        record.key(), record.value(),
                        record.partition(), record.offset());
            });

            consumer.commitAsync();
        }
        consumer.close();
        System.out.println("DONE");
		
		
	}

}
