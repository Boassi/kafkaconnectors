package br.com.itau.kafka.model;

import java.util.UUID;

public class Lancamento {

	private UUID idLancamento;
	
	private double valor;
	public Lancamento()
	{
	
	}
	public Lancamento(UUID idLancamento, double valor) {
		this.idLancamento = idLancamento;
		this.valor = valor;
	}

	public UUID getIdLancamento() {
		return idLancamento;
	}

	public void setIdLancamento(UUID idLancamento) {
		this.idLancamento = idLancamento;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
	
	
}
