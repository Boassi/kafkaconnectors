package br.com.itau.kafka.produtor;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProdutorJson {

	private String topic;
	private Properties props;
	private Producer producer;
	private ObjectMapper objectMapper;
	
	public ProdutorJson(String topic,Properties props)
	{
		this.props = props;
		this.topic = topic;
	}
	public boolean send(Object objeto)
	{
		producer = new KafkaProducer(this.props);
		objectMapper = new ObjectMapper();
	    JsonNode  jsonNode = objectMapper.valueToTree(objeto);
		producer.send(new ProducerRecord<String, JsonNode>(topic, jsonNode));
		producer.close();
		return false;			
	}
}
