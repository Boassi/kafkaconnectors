package br.com.itau.kafka.produtor;

import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class Produtor {
			
		private String topic;
		private Properties props;
		private Producer<String, String> producer;
		
		public Produtor(String topic,Properties props)
		{
			this.props = props;
			this.topic = topic;
		}
		public boolean send(String mensagem)
		{
			producer = new KafkaProducer<String, String>(this.props);
			producer.send(new ProducerRecord<String, String>(topic, mensagem));
			producer.close();
			return false;			
		}

		
		
		
}
